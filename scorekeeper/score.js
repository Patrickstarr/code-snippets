//Dry Code
// var button  = {
//   playerOneButton.document.getElementById("p1");
// }

//Reestablish variables
//Button selector
var playerOneButton = document.getElementById("p1");
var playerTwoButton = document.getElementById("p2");
var resetButton = document.getElementById("reset");

//Establish display
var p1SpanDisplay = document.querySelector("#displayP1");
var p2SpanDisplay = document.querySelector("#displayP2");


//gameInput
var numInput = document.querySelector("input");
var gameRounds = document.querySelector("p span");


//boolean score values
var scoreP1 = 0;
var scoreP2 = 0;
var isGameOver = false;
var winningScore = numInput;

playerOneButton.addEventListener("click", function(){
  if(!isGameOver){
    scoreP1++;
    if(scoreP1 === winningScore) {
      //Stop the game if isgameOver is true;
      isGameOver = true;
      p1SpanDisplay.classList.add("winner");
    }
    p1SpanDisplay.textContent  = scoreP1;
  }
});

playerTwoButton.addEventListener("click",function(){
  if(!isGameOver){
    scoreP2++;
    if(scoreP2 === winningScore){
      isGameOver = true;
      p2SpanDisplay.classList.add("winner");
    }
    p2SpanDisplay.textContent = scoreP2;
  }
});

resetButton.addEventListener("click",function(){
reset();
});

// make a separate function to pass it's value to other object method handlers
function reset() {
  scoreP1 = 0;
  scoreP2 = 0;
  p1SpanDisplay.textContent = 0;
  p2SpanDisplay.textContent = 0;
  p1SpanDisplay.classList.remove("winner");
  p2SpanDisplay.classList.remove("winner");
  isGameOver = false;
}

// a change event will run no matter what value is inside input
numInput.addEventListener("change",function(){
  // gameRounds = numInput;
  gameRounds.textContent = this.value;
  //convert the input value to a number value instead of displaying it as a string
  winningScore = Number(this.value);
  reset();

});
